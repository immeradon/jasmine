import 'dart:async';

import 'package:build/build.dart';
import 'package:jasmine_generator/src/default_proto.dart';
import 'package:source_gen/source_gen.dart';
import 'package:jasmine_generator/src/utils.dart';

class ClassBuilder extends Builder {
  final BuilderOptions options;
  ClassBuilder(this.options);

  @override
  Future build(BuildStep buildStep) async {
    final id = buildStep.inputId;

    final mainProtoBuffer = StringBuffer()
      ..writeln('syntax = "proto3";')
      ..write(defaultProtobuf);
    final mainDartBuffer = StringBuffer();

    await for (final library in buildStep.resolver.libraries) {
      final libraryReader = LibraryReader(library);

      final definedClasses = <String>[];
      for (final clazz in libraryReader.classes) {
        final className =
            "${library.name.toClassName}_${clazz.name.toClassName}";
        if (definedClasses.contains(className)) continue;
        definedClasses.add(className);

        final servicePbBuffer = StringBuffer();
        final pbBuffers = <StringBuffer>[];

        if (clazz.documentationComment != null)
          servicePbBuffer.writeln("${clazz.documentationComment}");
        servicePbBuffer.writeln("service $className {");
        servicePbBuffer.writeln("\t/// Register place for new class.");
        servicePbBuffer
            .writeln("\trpc registerForNewClass(Empty) returns (ClassId);");
        servicePbBuffer.writeln("\t/// Destroy the class.");
        servicePbBuffer.writeln("\trpc destoryClass(ClassId) returns (Bool);");
        servicePbBuffer.writeln("\t/// Check wether the class exists or not.");
        servicePbBuffer
            .writeln("\trpc doesClassExists(ClassId) returns (Bool);");

        int constructorNumber = 0;
        for (final constructor in clazz.constructors) {
          if (constructor.parameters.isEmpty) continue;
          constructorNumber++;
          if (constructor.isPrivate) continue;

          servicePbBuffer.writeln(
              "\t/// Constructor $constructorNumber: ${constructor.parameters}");
          if (constructor.documentationComment != null) {
            servicePbBuffer
                .writeln("\t/// ${constructor.documentationComment}");
          }
          for (final parameter in constructor.parameters) {
            final actionName =
                "constructor${constructorNumber}AddParameter${parameter.name.toClassName}";
            servicePbBuffer.writeln(
                "\trpc $actionName(${actionName.toClassName}Request) returns (ClassId);");

            final x = StringBuffer();
            x.writeln("message ${actionName.toClassName}Request {");
            x.writeln("\t${parameter.type} ${parameter.name} = 1;");
            x.writeln("}");
            pbBuffers.add(x);
          }
        }

        servicePbBuffer.writeln("}");

        pbBuffers.add(servicePbBuffer);
        pbBuffers.forEach((buffer) => mainProtoBuffer.write(buffer));
      }
    }

    await buildStep.writeAsString(
        id.changeExtension(".jasmine.class.proto"), mainProtoBuffer.toString());
    await buildStep.writeAsString(
        id.changeExtension(".jasmine.class.dart"), mainDartBuffer.toString());
  }

  @override
  Map<String, List<String>> get buildExtensions => {
        ".dart": [
          ".jasmine.class.proto",
          ".jasmine.class.dart",
        ],
      };
}
