extension StringExtensions on String {
  String get capitalize {
    if (isEmpty) return this;
    final firstChar = substring(0, 1);
    return firstChar.toUpperCase() + this.substring(1);
  }

  String get toClassName {
    var newString = "";
    for (final x in split(RegExp("[\._]"))) {
      newString += x.capitalize;
    }
    return newString;
  }
}
