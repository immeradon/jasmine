/// Support for doing something awesome.
///
/// More dartdocs go here.
// ignore_for_file: depend_on_referenced_packages

library jasmine_generator;

import 'package:build/build.dart';
import 'package:jasmine_generator/src/builders/class_builder.dart';

Builder classBuilder(BuilderOptions options) => ClassBuilder(options);
